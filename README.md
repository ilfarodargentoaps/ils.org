# Sito ILS

Questo è il repository del sito https://www.ils.org generato con [Jekyll](https://jekyllrb.com/).

## Sviluppare in locale

Esplora il repository. È strutturato in maniera piuttosto semplice.

In fase di sviluppo è possibile vedere in tempo reale le modifiche, eseguendo:

```
bundle exec jekyll serve --livereload
```

Quando hai finito, fai un commit e invia una pull request!

Grazie per ogni contributo!

## Aggiornare il sito (produzione)

Per aggiornare il sito vero e proprio non devi preoccuparti di nulla:

Quando i tuoi cambiamenti saranno uniti al ramo principale, il sito sarà aggiornato automaticamente, perché GitLab contatta servizi.linux.it, il quale fa la build del progetto (ramo principale) e la trasferisce in produzione. Qui la configurazione di GitLab:

https://gitlab.com/ItalianLinuxSociety/ils.org/-/blob/master/.gitlab-ci.yml

Nel dettaglio questo è quello che accade dopo che GitLab rileva una nuova commit nel ramo principale:

1. GitLab si collega via SSH al server servizi.linux.it eseguendo un comando remoto
2. GitLab lancia il comando `ssh` da una (nostra) immagine Docker (Debian + SSH client)
	* perché _Debian_? perchè è bene fidarsi (solo) di Debian in produzione
	* perché una _propria_ immagine se bastava `apt install ssh`? così ogni esecuzione è veloce & efficiente.
	* perché non delegare l'intera build a GitLab.com? non è bello dipendere troppo da servizi esterni.
3. nel server servizi.linux.it c'è l'utente `git-builder-ilsorg` che:
    1. usa `git` per scaricare/aggiornare il repository ufficiale in locale
    2. esegue Jekyll per compilare il sito statico nella directory `_site`
        1. per compilare il sito è utilizzata una immagine Docker già pronta con Ruby e Jekyll
    3. copia il sito statico appena compilato sul server di produzione tramite `rsync` (via SSH)
4. GitLab comunica il risultato di ogni passaggio (OK / FAIL) e messaggi di errore

Questo è lo script eseguito da servizi.linux.it per compilare e trasferire il sito in produzione:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/tree/main/cli

Questi sono i parametri di produzione:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/projects/ils.org/env-ilsorg-production.sh

Grazie per ogni contributo al processo di deploy!

## Segnalazioni

Per segnalazioni e richieste, apri una segnalazione:

https://gitlab.com/ItalianLinuxSociety/ils.org/-/issues/new

Oppure scrivi a:

webmaster@linux.it

Grazie!

## Licenza

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/

Eccezioni: loghi di associazioni, di partner e sponsor. Contattarli per conoscere le relative licenze.
