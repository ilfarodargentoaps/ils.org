---
layout: post
title: Linux Presentation Day
created: 1453127638
---
I linuxari italiani sono da tempo abituati al <a href="https://www.linuxday.it/">Linux Day</a>, la popolare manifestazione nazionale che da 15 anni mobilita l'intera community con decine di eventi che si svolgono nella medesima data a fine ottobre, ma recentemente l'iniziativa, da sempre prettamente nostrana, ha evidentemente "ispirato" altri entusiasti ed appassionati al di fuori del nostro Paese.

Negli scorsi due anni è stato instaurato il <a rel="nofollow" href="http://linux-presentation-day.org/">Linux Presentation Day</a>, attività molto simile al Linux Day per quanto riguarda l'organizzazione decentralizzata ma estesa in Germania, Austria e Svizzera ed oggi in fase di espansione presso altri Paesi europei. Per la prossima edizione, prevista per sabato 30 aprile, si stanno raccogliendo adesioni in Francia, Portogallo, Grecia, Regno Unito, e naturalmente si intende far sbarcare la manifestazione anche in Italia. Anzi: grazie all'intesa con i colleghi tedeschi l'edizione successiva del Linux Presentation Day europeo (che si tiene due volte all'anno, in primavera ed in autunno) sarà fatta convergere col Linux Day italiano, e sarà dunque fissata per sabato 22 ottobre.

Sulla <a rel="nofollow" href="http://www.linux-presentation-day.it/">pagina web dedicata all'implementazione italiana</a> si trovano tutti i dettagli e la pagina di registrazione, ed invitiamo tutti gli interessati a consultarla.

Le modalità di partecipazione raccomandate sono un poco diverse rispetto al solito, essendo il Linux Presentation Day incentrato sull'allestimento di banchetti e stand in luoghi pubblici e di transito presso cui distribuire materiale informativo o più semplicemente far toccare con mano le opportunità offerte da Linux e dal software libero ai passanti. Dunque, molto più semplice e facile da organizzare anche per gruppi meno numerosi o con scarse risorse a disposizione.

Attendiamo con ansia l'adesione di tutti, Linux Users Groups o gruppi spontanei, al primo vero "Linux Day europeo"!
