---
layout: post
title: Work in Progress su Linux.it
created: 1491156555
---
Circa quattro anni fa <a href="{% link _posts/2013-03-26-di-nuovo-online.md %}">è stata messa online</a> una completa revisione di <a href="https://www.linux.it/">linux.it</a>, da sempre destinato ad accogliere chi vuole avvicinarsi al mondo del software libero e guidarlo al meglio nel suo percorso di scoperta.

Questo, come i tanti altri siti tematici della rete ILS, viene costantemente monitorato ed occasionalmente ritoccato per rispondere alle domande più frequenti e comuni, ma l'aggiornamento pubblicato oggi merita una nota particolare. La homepage è stata in gran parte ridefinita per mettere in evidenza le pagine interne di maggiore interesse, che sono state a loro volta divise per categorie: quelle introduttive, quelle destinate a chi vuole effettivamente usare Linux sul proprio PC per la prima volta, quelle per chi già conosce il tema e vuole approfondire determinati argomenti, e quelle per chi vuole invece iniziare a partecipare e contribuire attivamente.

Una modifica piccola, ma destinata a condizionare le ricerche delle centinaia di visitatori che ogni singolo giorno dell'anno approdano su queste pagine per saperne qualcosa di più su Linux e sul software libero.
