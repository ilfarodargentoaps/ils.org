---
layout: post
title: LinuxSi Reloaded
created: 1579607047
---
Piccolo aggiornamento per <a href="https://linuxsi.com/" rel="nofollow">LinuxSi</a>, l'indice dei negozi Linux-friendly: da oggi saranno accolte non solo le segnalazioni di attività commerciali presso cui trovare computer con Linux preinstallato (o senza sistema operativo), ma anche negozi che - più genericamente - offrono assistenza e supporto a Linux, dall'installazione alla risoluzione di piccoli problemi.

Se da una parte il costo della licenza Windows - preinstallata e pagata da tutti sui nuovi computer, ma non richiesta da molti - può essere recuperato <a href="https://www.sistemainoperativo.it/">chiedendo un rimborso</a>, dall'altro la rete dei negozi Linux-friendly può essere una risorsa complementare a <a href="https://lugmap.linux.it/">quella dei Linux Users Groups</a>, presenti in molte città ma non dappertutto, nonché un prezioso canale di promozione e divulgazione nei confronti di Linux e del software libero, grazie anche all'adesivo <a href="{% link _posts/2016-09-03-vetrine-linuxsi.md %}">distribuito gratuitamente da Italian Linux Society</a> e da apporre sulla propria vetrina per incuriosire i passanti ed attirare nuovi clienti.

Spargete la voce, aiutateci ad arricchire l'indice nazionale, e a tenerlo aggiornato nel tempo con le vostre segnalazioni!
