---
layout: post
title: Sezioni Locali, Contributi e Siti Web
image: /assets/posts/images/sezionilocali2021.png
---

Man mano che si aggiungono nuove Sezioni Locali di Italian Linux Society si provvede anche a potenziarne i servizi per facilitarne ed accelerarne l'operatività sul territorio e permettere ai volontari di svolgere la loro opera di promozione e divulgazione del software libero.

<!--more-->

La maggiore novità introdotta è quella dei *grants*, per cui ogni Sezione può richiedere fino a **1000 euro all'anno** di contributo economico per coprire le spese delle proprie iniziative locali, che vanno a sommarsi alla parte delle quote di iscrizione ad ILS dei propri affiliati. Questo strumento è già stato messo in opera per supportare alcune Sezioni che, come tutti gli altri gruppi di volontari in Italia, hanno dovuto rinunciare all'organizzazione del proprio Linux Day cittadino nel 2020 ma si stanno preparando a ripartire con il [Linux Day 2021](https://www.linuxday.it/2021/) e tornare ad aggregare curiori ed appassionati nelle rispettive aree.

In secondo luogo, per semplificare gli aspetti più tecnici della promozione e della comunicazione da parte delle singole Sezioni, è stata predisposta una istanza Wordpress multi-site ed ogni Sezione può avere il proprio sito *città.ils.org* pronto all'uso, dotato del tema grafico ILS e già integrato con [l'aggregatore di news ed eventi di Planet ILS](/2021/07/16/eventi-planet-linux-it.html).

A oggi le Sezioni Locali sono attive [in 7 diverse regioni](/info#aderenti) (di cui 4 in capoluoghi) ed in soli due anni hanno rappresentato un notevole incremento nel numero di iscritti a ILS, permettendo di consolidare il ruolo di riferimento nazionale dell'associazione. Per maggiori informazioni sulle Sezioni Locali ILS, e per attivarne una nuova, è possibile consultare [questa pagina](/sezionilocali/).
