---
layout: post
title: Protocollo con Fondazione Edulife
image: /assets/posts/images/edulife_ils.png
---

È stato sottoscritto oggi un protocollo di intesa tra Italian Linux Society e [Fondazione Edulife](https://www.fondazioneedulife.org/), ente che si occupa di formazione e innovazione sociale.

<!--more-->

Negli ultimi mesi la Fondazione ha avuto modo non solo di scoprire ed approfondire i temi del software libero e open source, che sono strumentali per la promozione dei valori stessi su cui la fondazione opera, ma soprattutto di attivarsi sul proprio territorio di riferimento - quello di Verona - introducendo l'[Osservatorio e Laboratorio per l'Open Source](https://www.fondazioneedulife.org/olos/) e sostenendo l'avvio di un [percorso ITS](https://tuttoits.it/digitale-nuovo-corso-its-verona/) incentrato appunto su applicazioni libere.

Il protocollo con Italian Linux Society è propedeutico ad attività che si svolgeranno nei prossimi mesi, per coinvolgere non solo gli studenti ma anche le imprese dell'area veronese.
