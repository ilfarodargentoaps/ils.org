---
layout: post
title: 2016 con ILS
created: 1482881056
---
Anche quest'anno, un breve riassunto delle principali attività condotte da Italian Linux Society nel corso dei precedenti 12 mesi:

<ul>
  <li>lo storico <a href="https://scuola.linux.it/">Scuola.Linux.it</a> è stato interamente rinnovato, nella forma e nei contenuti, per rispecchiare più da vicino la realtà attuale della community freesoftware che orbita intorno alla scuola. Qui è possibile trovare occasionali aggiornamenti sul tema, utili links, e soprattutto i riferimenti ai <a href="https://scuola.linux.it/community">principali gruppi di lavoro</a> cui partecipare attivamente</li>
  <li>altri 12000 euro raccolti su <a href="http://donazioni.linux.it/">donazioni.linux.it</a> da impiegare per il sostegno economico dello sviluppo di software libero. Gran parte di questi sono stati destinati a <a rel="nofollow" href="http://sodilinux.itd.cnr.it/">So.Di.Linux</a>, la distribuzione Linux di riferimento per la scuola, ed alle funzionalità dedicate ai portatori di disabilità. Una nota: se siete interessati a lavorare sui progetti finanziati (e, ovviamente, essere retribuiti con i soldi appositamente raccolti) non esitate a <a href="/contatti">contattarci</a>: siamo alla ricerca di nuovi collaboratori!</li>
  <li>il nostro <a href="/progetti/manuale_operativo_lug.pdf">"Manuale Operativo per la Community"</a> ha subìto un massiccio aggiornamento, dedicato soprattutto alle realtà universitarie. Scopo del documento è fornire dritte e consigli per chi vuole instaurare un gruppo locale di promozione al software libero, e contribuire a promuovere Linux nella propria città (e, appunto, nella propria università!)</li>
  <li>abbiamo partecipato alla prima assemblea delle associazioni filo-linuxare in Europa, e ci apprestiamo a partecipare anche alla seconda. Presso cui porteremo, tra le altre cose, il progetto <a href="https://www.linux.it/radioattivi/">Radio-Attivi</a>, ispirato proprio da una campagna di Free Software Foundation Europe. Molte delle sfide che ci troviamo ad affrontare hanno una scala continentale e, in pieno spirito "opensource", confidiamo di collaborare sempre di più coi nostri vicini europei</li>
  <li>l'immancabile <a href="https://www.linuxday.it/">Linux Day</a>, la principale manifestazione italiana per la promozione di Linux e del software libero, che come sempre ha coinvolto migliaia di persone in circa 100 eventi distribuiti su tutto il territorio</li>
</ul>

Vi ricordiamo che per restare aggiornati su ciò che succede nel mondo del software libero in Italia potete sottoscrivere <a href="/newsletter">la nostra newsletter</a> o seguire <a rel="nofollow" href="https://twitter.com/ItaLinuxSociety/">il nostro account Twitter</a>. Nonché, ovviamente, associarvi alla nostra associazione e godere di <a href="/iscrizione">tutti i benefici riservati ai soci ILS</a>.
