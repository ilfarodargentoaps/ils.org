---
layout: post
title: Quattro Chiacchere con Federico Terzi
image: /assets/posts/images/interview.jpg
image_copy: Photo by <a href="https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Markus Winkler</a> on <a href="https://unsplash.com/s/photos/magnifier?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---

Abbiamo rivolto qualche domanda a Federico Terzi: classe '96, prolifico sviluppatore di software open source (come si evince anche [dal suo sito](https://federicoterzi.com/)) e maintainer di [Espanso](https://espanso.org/), uno dei progetti oggetto del giro di <a href="{% link _posts/2021-12-29-donazioni-2021.md %}">donazioni che ILS ha erogato a fine anno</a>.

<!--more-->

Come è nato Espanso?

> Espanso è nato due anni fa da una mia esigenza. Mi trovavo ad utilizzare tutti
> i principali sistemi operativi (Linux, Windows e macOS) e non esisteva una
> soluzione cross-platform per create delle scorciatorie testuali, come
> AutoHotKey su Windows e AutoKey su Linux.
>
> Allora mi sono messo a lavorare a Espanso, un software cross-platform che
> permette di creare potenti scorciatorie testuali usando file di configurazione
> (in puro stile Unix) e utilizzabili su tutti i più popolari sistemi operativi
> desktop.
>
> Il progetto è nato da subito open-source, perchè credo fortemente nei valori
> del software libero e dello knowledge sharing!

Cosa hai imparato da questa esperienza sia come sviluppatore che come persona?

> Costruire Espanso mi ha insegnato tanto. Dal punto di vista tecnico, mi ha
> permesso di imparare come gestire un progetto medio-grande dal punto di vista
> architetturale, nonchè molte tematiche legate alla programmazione di basso
> livello. A livello personale, mi ha regalato molte soddisfazioni, perchè
> giornalmente ricevo messaggi di ringraziamento da parte persone a cui Espanso
> ha migliorato la vita in un qualche modo. Se non fosse stato open source,
> questo non sarebbe stato possibile :)

Cosa ne pensi del panorama FOSS italiano e cosa si può fare per migliorarlo?

> Nonostante il numero di progetti open-source italiani sia relativamente
> limitato rispetto ad altri paesi, ho l'impressione che le cose stiano
> migliorando parecchio negli ultimi anni! Per chi è interessato, suggerisco
> anche [questa collection](https://github.com/collections/made-in-italy).
>
> Oltre alle ottime iniziative promosse dalla ILS, ci sono stati anche degli
> eventi molto interessanti legati all'open-source, come l'Open Source Day di
> Firenze promosso dai ragazzi di [Schrödinger Hat](https://www.schrodinger-hat.it/), che approfitto per salutare!
>
> Sono molto ottimista riguardo al futuro dell FOSS italiano!

Come contribuire a Espanso?

> Abbiamo da poco rilasciato la version 2.0 di Espanso, e ci sono molte cose su
> cui è possibile aiutare! Ci stiamo ancora stabilizzando sulle procedure per
> favorire le contribution, quindi suggerisco a chi fosse interessato di seguire
> lo [sviluppo su GitHub](https://github.com/federico-terzi/espanso), ci saranno notizie a breve!

Ci auguriamo che l'esperienza di Federico possa essere di aiuto ed ispirazione per altri giovani, che nello sviluppo di software libero e open source possono facilmente trovare una opportunità di crescita personale e professionale.
