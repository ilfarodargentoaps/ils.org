---
layout: post
title: Campus Party Italia 2018
created: 1532995838
---
<p style="text-align: center">
    <img src="/assets/posts/images/cpit2.jpg" alt="ILS a Campus Party Italia 2018" class="img-fluid">
</p>

<p>
    Dal 18 al 21 luglio Italian Linux Society è stata a <a rel="nofollow" href="http://italia.campus-party.org/">Campus Party</a>, manifestazione dedicata all'innovazione digitale svoltasi presso la Fiera di Milano.
</p>
<p>
    In tali giorni abbiamo presieduto lo spazio con un nostro stand, distribuendo adesivi, confrontandoci con altre realtà più o meno direttamente legate al mondo del software libero (tra cui diversi Linux Users Groups), informando il pubblico di passaggio (inclusi molti giovani) sulle opportunità offerte da Linux e dall'opensource, e promuovendo diverse attività collaterali (tra cui, ovviamente, <a href="https://www.linuxday.it/2018/">il prossimo Linux Day</a>).
</p>
<p>
    Sabato 21 si è tenuto <a rel="nofollow" href="https://campuse.ro/events/campus-party-italia-2018/talk/open_source/">un talk</a> da parte del presidente, Roberto Guido, incentrato sulla campagna <a href="https://www.linux.it/unopercento">UnoPercento</a>: i partecipanti sono stati sollecitati a destinare una parte del proprio fatturato al sostegno dei progetti liberi e open con cui quotidianamente lavorano, al fine di garantirne la crescita e la manutenzione nel tempo.
</p>
