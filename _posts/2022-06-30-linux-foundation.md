---
layout: post
title: ILS aderisce a Linux Foundation
image: /assets/posts/images/lf_ils.png
---

Italian Linux Society ha aderito a [Linux Foundation](https://www.linuxfoundation.org/), ente di portata globale e ben noto a chiunque conosce la realtà (soprattutto professionale) di Linux e dell'open source.

<!--more-->

Nata nel 2000 per supportare lo sviluppo del kernel Linux, oggi la Foundation è il contenitore di innumerevoli progetti open source e di innumerevoli iniziative volte a standardizzare, razionalizzare ed uniformare il variegato ecosistema tecnologico libero, cui migliaia di aziende di ogni dimensione partecipano ed attingono per costruire prodotti ed offrire soluzioni alternative a quelle delle classiche "Big Tech" statunitensi.

Italian Linux Society, nell'unirsi alle [centinaia di altri soggetti](https://www.linuxfoundation.org/our-members-are-our-superpower-2/) che sostengono Linux Foundation, ne condivide gli intenti e gli obiettivi. Tra cui ad esempio quello di far crescere e sviluppare competenze professionali nell'ambito degli strumenti e della metodologia open source, che - stando al recente ["Open Source Jobs Report"](https://training.linuxfoundation.org/resources/2022-open-source-jobs-report/) pubblicato da Linux Foundation stessa - risultano ancora estremamente scarse rispetto alla domanda in costante aumento da parte dell'industria.

Se anche la tua azienda lavora con soluzioni libere e open source e vuole aiutarci a consolidare questa realtà in Italia, [sostieni Italian Linux Society](/sponsorizzazioni)!
