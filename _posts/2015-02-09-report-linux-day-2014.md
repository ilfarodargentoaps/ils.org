---
layout: post
title: 'Report: Linux Day 2014'
created: 1423492933
---
Sabato 25 ottobre 2014 si è svolta la quattordicesima edizione del <a href="https://www.linuxday.it/">Linux Day</a>, la principale manifestazione italiana sul software libero. E negli scorsi mesi sono stati erogati due distinti questionari, uno agli organizzatori delle quasi 100 istanze locali dell'iniziativa ed uno a coloro che vi hanno partecipato in veste di visitatori, con lo scopo di analizzare e misurare l'impatto che anche quest'anno l'evento ha avuto.

Pubblichiamo oggi i risultati raccolti, miscelati nel <a href="/assets/files/RapportoLinuxDay2014.pdf">Rapporto Linux Day 2014</a>, che presenta entrambi i punti di vista per una visione di insieme completa. Nel bene e nel male, e con qualche considerazione finale sul futuro dell'iniziativa.

Approfittiamo per ricordare che il prossimo Linux Day si svolgerà sabato 24 ottobre 2015, come sempre in tutta Italia. Per aggiornamenti è possibile seguire <a href="https://www.linuxday.it/">il sito web dedicato</a>, <a rel="nofollow" href="https://twitter.com/linuxdayitalia">l'account Twitter</a>, <a rel="nofollow" href="https://www.facebook.com/LinuxDayItalia">la pagina Facebook</a>, <a rel="nofollow" href="https://plus.google.com/+linuxday/posts">la pagina Google+</a>, o <a href="https://lists.linux.it/listinfo/linuxday-idee">la mailing list di coordinamento</a>.
