---
layout: post
title: 'Diritto al Rimborso: Interpellati i Produttori'
created: 1424716715
---
La campagna divulgativa sul rimborso delle licenze Windows sui PC di nuovo acquisto, lanciata per accompagnare <a href="{% link _posts/2014-10-17-diritto-al-rimborso.md %}">la lettera</a> firmata insieme all'Associazione Diritti Utenti e Consumatori e Free Software Foundation Europe e destinata all'Autorità Garante per la Concorrenza ed il Mercato, ha avuto un discreto successo ed ha permesso di informare molti sul diritto sancito in settembre dalla Corte di Cassazione. Ma come <a href="{% link _posts/2014-12-30-un-anno-con-ils.md %}">anticipato</a> c'è ancora molto da fare prima che tale diritto sia reso effettivo, ed i primi a dover essere coinvolti sono i produttori che tale rimborso lo devono all'atto pratico garantire ed erogare.

Abbiamo pertanto stamane inviato una lettera a quattro dei maggiori distributori di computer in Italia, con l'intento di sollecitare loro una dichiarazione ed una esplicita posizione sulla vicenda. In modo da poter informare il pubblico sulle loro intenzioni, e permettere a tutti di regolarsi di conseguenza quando sarà il momento di acquistare un nuovo PC.

La lettera è <a href="https://www.sistemainoperativo.it/index.html#appello">pubblicata sul sito della campagna</a>, su cui verranno esposte anche le risposte ricevute (o verrà evidenziata la mancanza di una risposta...). Vi raccomandiamo di continuare a condividere e diramare l'appello sui social network, e a <a rel="nofollow" href="http://www.agcm.it/invia-segnalazione-online.html">segnalare ad AGCM</a> eventuali problemi riscontrati durante l'esercizio del proprio diritto.
