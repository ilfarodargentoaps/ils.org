---
layout: post
title: 'Appello per i Comuni: Condividete'
created: 1497435765
---
<p style="text-align: center">
<img src="/assets/posts/images/comunefromscratch.png" alt="Comune From Scratch" style="max-width: 90%; margin: 10px 0">
</p>

<p>
Tra i tanti comuni italiani che sono stati chiamati al voto l’11 giugno uno ha la particolarità di averlo fatto per la prima volta. <a rel="nofollow" href="https://it.wikipedia.org/wiki/Mappano">Mappano</a>, in provincia di Torino, è stato promosso dallo status di “frazione” a quello di “comune” solo nel 2013, ed ha ora per la prima volta eletto un proprio consiglio comunale. Cui spetta il non facile compito di costruire una nuova amministrazione da zero, ma con la rara opportunità di poterlo fare senza vincoli pregressi.
</p>
<p>
Il neo eletto Consiglio ha la necessità di allestire una propria infrastruttura informatica e di dotarsi di servizi digitali, sia per i cittadini che per il suo stesso funzionamento interno, ed in ossequio delle linee guida del <a rel="nofollow" href="http://www.agid.gov.it/cad/codice-amministrazione-digitale">Codice di Amministrazione Digitale</a> ha deciso di adottare soluzioni libere ed opensource e riusare quanto già sviluppato da altre amministrazioni simili.
</p>
<p>
Il lodevole intento è purtroppo ostacolato dalla mancanza di informazioni e riferimenti: a distanza di 11 anni dalla pubblicazione del suddetto Codice risulta ancora difficile, se non impossibile, reperire soluzioni libere e riusabili specifiche per la PA. Il <a rel="nofollow" href="http://www.agid.gov.it/catalogo-nazionale-programmi-riusabili">Catalogo Nazionale dei Programmi Riutilizzabili</a>, oggi in carico all’<a rel="nofollow" href="http://www.agid.gov.it/">Agenzia per l’Italia Digitale</a>, è scarsamente curato ed in massima parte riporta riferimenti ad applicazioni proprietarie o non più reperibili, né esiste una rete - ufficiale o ufficiosa - di soggetti cui rivolgersi.
</p>
<p>
Il Comune di Mappano, insieme ad <a href="/">Italian Linux Society</a>, lancia dunque <a rel="nofollow" href="https://www.linux.it/comune-from-scratch">un appello a tutti i comuni per raccogliere soluzioni ad-hoc per l’amministrazione</a>: protocollo, anagrafe, gestione personale, servizi demografici e finanziari.
</p>
<p>
I comuni italiani con meno di 10.000 abitanti, e dunque con risorse e competenze interne troppo modeste per potersi autonomamente e rapidamente dotare di una completa struttura digitale, sono 6.700 su un totale di 8.000: obiettivo dell’iniziativa è quello di indicizzare in modo più efficace e conveniente il patrimonio software di pubblica utilità in uso e renderlo più facilmente accessibile a queste realtà, nonché di dare supporto tecnico ai comuni che hanno internamente implementato le proprie soluzioni e vogliono condividerle con gli altri.
</p>
