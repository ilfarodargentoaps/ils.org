---
layout: post
title: Diritto al Rimborso
created: 1413543170
---
La notizia della sentenza della Cassazione secondo cui i consumatori hanno <a rel="nofollow" href="http://avvertenze.aduc.it/rimborsowindows/articolo/windows+preinstallato+cassazione+vittoria_22491.php">diritto al rimborso</a> delle licenze Windows acquistate in modo coatto insieme ai nuovi computer ha avuto ampia risonanza su tutti i media, non solo quelli dedicati al settore informatico. Ma è adesso ora di pretendere che tale diritto sia fatto rispettare da parte dei produttori, che devono garantire chiare procedure di richiesta, modalità non inutilmente complesse, e compensi equi ed allineati al mercato.

E' per questo che oggi l'<a rel="nofollow" href="http://www.aduc.it/">Associazione per i Diritti degli Utenti e dei Consumatori</a>, insieme a <a rel="nofollow" href="https://fsfe.org/">Free Software Fondation Europe</a> e <a href="/">Italian Linux Society</a>, ha presentato all'Autorità Garante per la Concorrenza ed il Mercato (AGCM) <a rel="nofollow" href="http://www.aduc.it/comunicato/rimborso+windows+preinstallato+aduc+denuncia_22570.php">una esplicita richiesta</a> ad intervenire nei confronti del reiterato ostruzionismo deliberatamente operato dai produttori, che mai si sono premurati di informare, assistere ed assecondare i consumatori nell'esercizio della legittima pratica.

Per l'occasione Italian Linux Society ha pubblicato la campagna divulgativa <a href="https://www.sistemainoperativo.it/">"Fatti Rimborsare il Sistema (in)Operativo!"</a>, da condividere e propagare ad amici e conoscenti, mirata a sollecitare i produttori ad esprimersi in modo chiaro sulla questione. Perché una volta sancito il diritto al rimborso è ora indispensabile renderlo attuabile per davvero: un diritto nascosto e osteggiato di fatto non esiste.

Grazie ad ADUC per l'opera sinora svolta a tutela della libertà di tutti!
