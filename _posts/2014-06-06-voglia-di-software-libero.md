---
layout: post
title: Voglia di Software Libero
created: 1402011769
---
Il Ministero per la Semplificazione <a rel="nofollow" href="http://www.funzionepubblica.gov.it/TestoPDF.aspx?d=33571">ha pubblicato qualche dato</a> sulle mail ricevute a seguito dell'appello lanciato il mese scorso al fine di coinvolgere i cittadini con idee e proposte finalizzate a migliorare la funzione ed il funzionamento della Pubblica Amministrazione in Italia. E salta all'occhio come tra i temi più spesso citati spontaneamente, al di fuori degli argomenti proposti, si trovi anche il software libero, presente in più di 1000 messaggi (compreso <a href="{% link _posts/2014-05-28-la-nostra-lettera-al-governo.md %}">quello di Italian Linux Society</a>) indirizzati negli ultimi 30 giorni a rivoluzione@governo.it
Un numero che lascia pochi dubbi sulla diffusione e sulla capillarità del "fenomeno opensource", evidentemente sentito e percepito da una fetta consistente di cittadini a loro volta sempre più coscienti ed attenti al ruolo che la tecnologia ha nella nostra vita quotidiana.

Il software libero e opensource, che per sua natura è interoperabile, trasparente ed economicamente efficiente, è e deve essere lo strumento e la metodologia d'elezione con cui implementare la digitalizzazione di una Pubblica Amministrazione altrettanto interoperabile, trasparente ed efficiente. Le disposizioni normative in tal senso non mancano, la volontà popolare (evidentemente) neppure, è ora necessaria l'intenzione politica - condita da competenza e consapevolezza tecnica - per rendere operativi i proclami di innovazione e cambiamento.

Ci auguriamo che questa massiccia risposta pubblica venga interpretata come un segnale da parte del Governo, una presa di posizione che non può essere ignorata né tantomeno rimandata ulteriormente.
