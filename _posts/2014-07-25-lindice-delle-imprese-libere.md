---
layout: post
title: L'Indice delle Imprese "Libere"
created: 1406323961
---
Da pochi giorni <a rel="nofollow" href="http://www.lpi-italia.org/">Linux Professional Institute</a>, uno dei principali punti di riferimento nel mondo del software libero applicato all'impresa, ha avviato <a rel="nofollow" href="http://www.lpi-italia.org/2014/07/16/arriva-in-italia-programma-certified-solution-provider/">una importante campagna</a> di "indicizzazione" delle aziende dedite a fornire servizi, formazione e consulenza su Linux e su soluzioni open, con l'obiettivo di rendere più facile ed agevole il contatto tra la domanda di chi ha intenzione di avvalersi degli innumerevoli vantaggi del software e codice aperto (trasparenza, interoperabilità, alti margini di personalizzazione ed assenza di logiche di lock-in sugli applicativi usati) e l'offerta degli esperti del settore.

L'iniziativa è stata lanciata in un momento storico di particolare interesse, in cui un numero sempre maggiore di realtà (sia pubbliche che private, anche di grandi dimensioni) hanno compiuto o stanno per compiere il passaggio - tecnologico, ma anche culturale ed operativo - al software libero, ma non sempre riescono ad individuare chi possa fornire l'assistenza necessaria per compiere migrazioni su ampia scala. I contenuti del nuovo indice saranno verificati e valutati dal team di Linux Professional Institute, al fine di garantire una elevata qualità tecnica degli enti referenziati ed una reale adesione degli stessi ai principi etici del movimento freesoftware.

Ci auguriamo che la campagna possa avere un ampio riscontro, e fornire una ulteriore accelerazione a Linux ed al software "free libre open source" nel nostro Paese.
